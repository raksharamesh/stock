import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
import './App.css';
// import components
import Landing from './components/pages/Landing';
import Login from './components/pages/Login';
import Nav from './components/tools/Nav';
import Banners from './components/tools/Banner';
import { LogoutButton, StoreButton } from './components/tools/Buttons';
import StoreLocate from './components/tools/StoreLocate';
import StoreStock from './components/tabs/StoreStock';
import ReprintLabels from './components/tabs/ReprintLabels';
import ResendOrders from './components/tabs/ResendOrders';
import CashOrders from './components/tabs/CashOrders';
import KioskControl from './components/tabs/KioskControl';
import FAQ from './components/tabs/FAQ';
// import context
import { usePaddingTop, useStoreID, useCHAIN, useMODE } from './components/Context';
// import data
import configData from "./data/config.json"
// import functions
import { GETwebsocketStatus } from './functions/APIFunctions';
import { doesValueExist } from './functions/DataHelper';

export default function App() {
    const [state, setState] = useState("");
    const [locationID, setLocationID] = useState("");
    // flag
    const [isLogin, setIsLogin] = useState(false);
    const [storeSelect, setStoreSelect] = useState(false);
    const [noInternet, setNoInternet] = useState(false);
    const [printerWarn, setPrinterWarn] = useState(false);


    const { paddingTop, setPaddingTop } = usePaddingTop();
    const { storeID, setStoreID } = useStoreID();
    const { CHAIN, setCHAIN } = useCHAIN();
    const { MODE, setMODE } = useMODE();
    // check if page is refreshed then redirect to home page
    const navigate = useNavigate();

    useEffect(() => {
        if (window.performance.getEntriesByType('navigation').map((nav) => nav.type).includes('reload')) {
            navigate("/");
        };
    }, []);

    // reset all states when user logs out
    useEffect(() => {
        if(!isLogin) {
            reset();
        };
    }, [isLogin]);

    useEffect(() => {
        if (storeID && CHAIN && MODE) {
            setLocationID(configData[MODE][CHAIN]["LOCATION_ID"][storeID]);
        };
    }, [storeID, CHAIN, MODE]);

    // periodic checks
    // check network connection
    useEffect(() => {
        networkCheck();
        const interval = setInterval(networkCheck, 10000); // ping every 10 seconds

        // clear interval on component unmount
        return () => clearInterval(interval);
    }, []);

    // check kitchen printer connection
    useEffect(() => {
        const callWebsocketAPI = async () => {
            const websocketStatus = await GETwebsocketStatus(CHAIN, MODE);
            if (websocketStatus) {
                setPrinterWarn(!doesValueExist(websocketStatus[locationID], "KitchenPrinter"));
            };
        };
        callWebsocketAPI();
        const interval = setInterval(callWebsocketAPI, 10000);

        // clear interval on component unmount
        return () => clearInterval(interval);
    }, [locationID]);

    useEffect(() => {
        if (noInternet || printerWarn) {
            setPaddingTop(15);
            if (noInternet && printerWarn) {
                setPaddingTop(20);
            };
        } else {
            setPaddingTop(10);
        };
    }, [noInternet, printerWarn]);

    ////////////////////
    // RESET FUNCTION //
    ////////////////////
    function reset() {
        setState("");
        setStoreID(undefined);
        setMODE("");
        setCHAIN("");
        // flag
        setIsLogin(false);
        setStoreSelect(false);
    };

    function networkCheck() {
        if (navigator.onLine) {
            setNoInternet(false)
        } else {
            setNoInternet(true);
        };
    };


    /////////////////
    // COMPONENTS //
    ////////////////
    let loginPage = <Login sendLoggedin={setIsLogin} sendStoreSelect={setStoreSelect} sendState={setState} />

    let buttons = <div className="flexParent buttonContainerRight" style={{ paddingTop: `${paddingTop - 10}vh` }}>
                    { storeSelect ? <StoreButton sendState={setState} /> : null}
                    <LogoutButton sendLogout={setIsLogin} />
                </div>
                
    let storeLocate = <>
                        <div className="buttonContainerRight" style={{ paddingTop: `${paddingTop - 10}vh` }}>
                            <LogoutButton sendLogout={setIsLogin} />
                        </div>
                        <StoreLocate />
                    </>

    let stockManage = <>
                        { buttons }
                        <StoreStock />
                    </>
    
    let printManage = <>
                            { buttons }
                            <ReprintLabels />
                        </>

    let KDSManage = <>
                        { buttons }
                        <ResendOrders />
                    </>

    let cashManage = <>
                        { buttons }
                        <CashOrders />
                    </>

    let kioskManage = <>
                        { buttons }
                        <KioskControl />
                    </>

    let FAQPage = <>
                      <FAQ/>
                  </>

    // dynamic post-login page (based on credientials)
    let postLogin = <></>
    if (storeSelect) {
        postLogin = storeLocate;
    } else {
        postLogin = stockManage;
    };


    return (
      <>
        { isLogin && storeID !== undefined ? <Nav newState={state} sendState={setState} /> : null }
        { isLogin && <Banners noInternet={noInternet} printerWarn={printerWarn} /> }
        <Routes>
            <Route exact path="/" element={ <Landing /> } />
            <Route exact path="/login" element={ isLogin ? postLogin : loginPage } />
            <Route path="/Manage" element={ isLogin ? storeLocate : loginPage } />
            <Route path="/Stock" element={ isLogin ? stockManage : loginPage } />
            <Route path="/Print" element={ isLogin ? printManage : loginPage } />
            <Route path="/KDS" element={ isLogin ? KDSManage  : loginPage } />
            <Route path="/Cash" element={ isLogin ? cashManage  : loginPage } />
            <Route path="/Kiosk" element={ isLogin ? kioskManage  : loginPage } />
            <Route path="/FAQ" element={ isLogin ? FAQPage : loginPage } />
        </Routes>
      </>
    );
}
