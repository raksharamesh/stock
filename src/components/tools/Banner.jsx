import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";

export default function Banners({ noInternet, printerWarn }) {
    return (
        <div className="bannerContainer">
            { noInternet && <CriticalBanner /> }
            { printerWarn && <WarningBanner /> }
        </div>
    )
};

export function CriticalBanner() {
    return (
        <div className="bannerCritical">
            <div className="flexParent">
                <FontAwesomeIcon className="textLarge textLeft textWhite bold" icon={faTriangleExclamation} />
                <p className="text textLeft textWhite bold">&nbsp; CRITICAL ISSUE: No Internet Connection</p>
            </div>
        </div>
    )
};

export function WarningBanner() {
    return (
        <div className="bannerWarn">
            <div className="flexParent">
                <FontAwesomeIcon className="textLarge textLeft bold" icon={faTriangleExclamation} />
                <p className="text textLeft bold">&nbsp; WARNING: Kitchen Printer is Disconnected</p>
            </div>
        </div>
    )
};