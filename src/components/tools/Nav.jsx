import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faPrint, faTabletScreenButton, faMoneyBill1Wave, faDisplay, faQuestion } from "@fortawesome/free-solid-svg-icons";
// import data
import featuresData from "../../data/features.json"
// import context
import { useCHAIN } from "../Context";

const navTitles = {
    'Stock': {
        "Name": 'Stock Management',
        "Icon": null
    },
    'Print': {
        "Name": 'Label Printer',
        "Icon": faPrint
    },
    'KDS': {
        "Name": 'Kitchen Display',
        "Icon": faTabletScreenButton
    },
    'Cash': {
        "Name": 'Cash Orders',
        "Icon": faMoneyBill1Wave
    },
    'Kiosk': {
        "Name": 'Kiosk',
        "Icon": faDisplay
    },
    'FAQ': {
        "Name": 'FAQ',
        "Icon": faQuestion
    }
};

export default function Nav({ newState, sendState }) {
    const [state, setState] = useState(newState);
    const [featureList, setFeatureList] = useState([]);

    const { CHAIN } = useCHAIN();

    // update state
    useEffect(() => {
        setState(newState);
    }, [newState]);
    console.log("IN NAV:", state);

    useEffect(() => {
        setFeatureList(featuresData[CHAIN]);
    }, [CHAIN])

    // function to send state to parent and update state
    function clickNav(state) {
        sendState(state);
        setState(state);
    };

    let navSections = Object.keys(navTitles).map(k => {
        // only show features present in featureList
        if (featureList.includes(k)) {
            let style = "flex1 navSection buttonBlank"
            if (k === state) {
                style = "flex1 navSection navSectionSelected buttonBlank"
            };

            let displayName = navTitles[k]['Name']
            let icon = navTitles[k]['Icon']
            return (
                <Link className={style} onClick={() => clickNav(k)} to={"/" + k}>
                    <div >
                        <p className="subtitle textWhite">
                            <span>{displayName}</span>
                            <span style={{ marginLeft: '5px'}}><FontAwesomeIcon icon={icon} /></span>
                        </p>
                    </div>
                </Link>
            )
        };
    });

    return (
        <div className="navContainer flexParent">
            { navSections }
        </div>
    )
};