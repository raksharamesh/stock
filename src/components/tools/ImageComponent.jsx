import React, { useState, useEffect } from 'react';

const ImageComponent = ({ src, alt }) => {
    const [imageSrc, setImageSrc] = useState(null);

    useEffect(() => {
        const loadImage = async () => {
            try {
                const imagePath = await import(`./images/${src}`);
                setImageSrc(imagePath.default);
            } catch (error) {
                console.error("Error loading image:", error);
            }
        };
        loadImage();
    }, [src]);

    return imageSrc ? <img src={imageSrc} alt={alt} /> : null;
};

export default ImageComponent;
