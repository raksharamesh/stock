import React, { useState, useEffect } from "react";
// import data
import configData from "../../data/config.json";
import storeData from "../../data/stores.json";
import { usePaddingTop, useStoreID, useCHAIN } from '../Context';


// define MODE
const MODE = configData['MODE']; // test or prod

export default function StoreLocate() {
    const [storeList, setStoreList] = useState({});

    const { storeID, setStoreID } = useStoreID();
    const { CHAIN } = useCHAIN();
    const { paddingTop } = usePaddingTop();

    // set storeList according to chain
    useEffect(() => {
        setStoreList(storeData[CHAIN]);
    }, [CHAIN])


    let storeDivs = Object.keys(storeList).map((k) => {
        // do not show dev store in prod mode
        if (MODE === "prod" && k === "dev") {
            return;
        } else if (k === storeID) {
            return (
                <div id={k} className="storeDiv bgOrange" onClick={() => setStoreID(k)}>
                    <p className="textMed bold textWhite">{storeList[k]}</p>
                </div>
            )
        } else {
            return (
                <div id={k} className="storeDiv" onClick={() => setStoreID(k)}>
                    <p className="textMed bold textWhite">{storeList[k]}</p>
                </div>
            )
        };
    });

    let allStores = <>
                        <p className="textLarge bold textLeft spacingTop3 spacing2">Select your store location</p>
                        <div className="storeGrid">
                            { storeDivs }
                        </div>
                    </>

    return (
        <div className="dashboardContainer" style={{ paddingTop: `${paddingTop}vh` }}>
            { allStores }
        </div>
    )
};