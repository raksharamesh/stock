import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faRightFromBracket } from "@fortawesome/free-solid-svg-icons";
// import data
import storeList from "../../data/stores.json";
// import context
import { useStoreID } from "../Context";

export function LogoutButton({ sendLogout }) {
    return (
        <Link to="/" onClick={() => sendLogout(false)}>
            <button className="buttonBlank buttonLogout">
                <p className="subtitle textWhite">
                    <span className="textWhite">Logout&ensp;</span>
                    <span className="textWhite"><FontAwesomeIcon icon={faRightFromBracket} /></span>
                </p>
            </button>
        </Link>
    )
};

export function StoreButton({ sendState }) {
    const [storeName, setStoreName] = useState("");

    const { storeID } = useStoreID();

    // update storeID
    useEffect(() => {
        setStoreName(storeList[storeID]);
    }, [storeID]);

    return (
        <Link to="/Manage" onClick={() => sendState("")}>
            <button className="buttonBlank buttonLogout">
                <p className="subtitle textWhite">Store</p>
                <p className="text textAuto textWhite">{storeName}</p>
            </button>
        </Link>
    )
};