import { useState, useEffect, useContext, createContext } from "react";

// create parent provider
export function AppProviders({ children }) {
    return (
        <MODEProvider>
            <CHAINProvider>
                <StoreIDProvider>
                    <PaddingTopProvider>
                        { children }
                    </PaddingTopProvider>
                </StoreIDProvider>
            </CHAINProvider>
        </MODEProvider>
    );
};

//////////////
// PADDING //
/////////////
export const PaddingTopContext = createContext(null);

export function PaddingTopProvider({ children }) {
    const [paddingTop, setPaddingTop] = useState(0);
    
    return (
        <PaddingTopContext.Provider value={{ paddingTop, setPaddingTop }}>
            { children }
        </PaddingTopContext.Provider>
    );
};

export function usePaddingTop() {
    const info = useContext(PaddingTopContext);
    if (!info) {
        throw new Error('usePaddingTop must be used within a PaddingTopContext');
    }
    return info
};


///////////////
// STORE ID //
//////////////
export const StoreIDContext = createContext(null);

export function StoreIDProvider({ children }) {
    const [storeID, setStoreID] = useState(undefined);
    
    return (
        <StoreIDContext.Provider value={{ storeID, setStoreID }}>
            { children }
        </StoreIDContext.Provider>
    );
};

export function useStoreID() {
    const info = useContext(StoreIDContext);
    if (!info) {
        throw new Error('useStoreID must be used within a StoreIDContext');
    };
    return info
};

//////////////////
// STORE CHAIN //
/////////////////
export const ChainContext = createContext(null);

export function CHAINProvider({ children }) {
    const [CHAIN, setCHAIN] = useState("");
    
    return (
        <ChainContext.Provider value={{ CHAIN, setCHAIN }}>
            { children }
        </ChainContext.Provider>
    );
};

export function useCHAIN() {
    const info = useContext(ChainContext);
    if (!info) {
        throw new Error('useCHAIN must be used within a ChainContext');
    };
    return info
};

///////////
// MODE //
//////////
export const ModeContext = createContext(null);

export function MODEProvider({ children }) {
    const [MODE, setMODE] = useState("");
    
    return (
        <ModeContext.Provider value={{ MODE, setMODE }}>
            { children }
        </ModeContext.Provider>
    );
};

export function useMODE() {
    const info = useContext(ModeContext);
    if (!info) {
        throw new Error('useMODE must be used within a ModeContext');
    };
    return info
};