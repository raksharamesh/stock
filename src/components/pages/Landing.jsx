import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import your icons
import { faCircleChevronLeft, faCircleChevronRight } from '@fortawesome/free-solid-svg-icons'
// import logo
import logo from '../../image/logo.png';

export default function Landing({ }) {
    const [currdeg, setCurrdeg] = useState(0);

    // define carousel item info
    const cardInfo = ["Display Control", "Supply Status", "Dashboard", "Management", "Customers", "Store Info", "Reporting"];
    let cardNum = cardInfo.length;
    let degTurn = 360 / cardNum;
    let items = document.querySelectorAll("div.card");

    useEffect(() => {
        rotateItems()
        rotateCar()
    }, [currdeg]);


    ////////////////
    // FUNCTIONS //
    ///////////////
    // turn carousel
    function rotateCar() {
        const car = document.getElementById("carousel");
        car.style.transform = "rotateY("+currdeg+"deg)";
    };
    // turn individual items
    function rotateItems() {
        Object.keys(items).map((i) => {
            items[i].style.transform = "rotateY("+(-currdeg)+"deg)";
        });
    };

    ////////////////////
    // CAROUSEL ITEMS //
    ///////////////////
    let carouselItems = cardInfo.map((k, i) => {
        let dynamicTransform = `rotateY(${degTurn * i}deg) translateZ(${cardNum * 45}px) rotateY(-${degTurn * i}deg)`;
        let image = k + '.png';

        return (
            <div className="preserve3d" style={{ transform: dynamicTransform }}>
                <div className="card preserve3d">
                    <p className="spacingTop5 spacing2 text textDark cardText">{k}</p>
                    <img className="cardImage" src={require('../../image/' + image)} alt="" />
                </div>
            </div>
        )
    });

    return (
        <>
            <div className="landingContainer">
                <div className="div20">
                    <img className="logoImage" src={logo} />
                    <p className="title">Management Portal</p>
                </div>

                <div className="flexParent">
                    <div className="flex1 centerVertical index2" onClick={() => setCurrdeg(currdeg + degTurn)}>
                        <FontAwesomeIcon className="arrowIcon prev" icon={ faCircleChevronLeft } />
                    </div>
                    
                    <div className="flex1 index1">
                        <div className="carouselContainer">
                            <div id="carousel">
                                { carouselItems }
                            </div>
                        </div>
                    </div>

                    <div className="flex1 centerVertical index2" onClick={() => setCurrdeg(currdeg - degTurn)}>
                        <FontAwesomeIcon className="arrowIcon next" icon={ faCircleChevronRight } />
                    </div>
                </div>

            </div>
            <div className="loginBottom">
                <Link to="/login">
                    <button className="div20 textUser textWhite loginBottomButton">Log In</button>
                </Link>
            </div>
        </>
    )
};