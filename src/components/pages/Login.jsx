import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import icons
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
// import logo
import logo from '../../image/logo.png';
// import data
import configData from "../../data/config.json";
// import context
import { useStoreID, useCHAIN, useMODE } from "../Context";

// set login URL
const baseURL = configData["LOGIN_URL"]; 
const backendURL = baseURL + "login";

export default function Login({ sendLoggedin, sendStoreSelect, sendState }) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    // flag
    const [showPassword, setShowPassword] = useState(false);
    const [usernameWarn, setUsernameWarn] = useState(false);
    const [passwordWarn, setPasswordWarn] = useState(false);
    const [postFail, setPostFail] = useState(false);

    const { setStoreID } = useStoreID();
    const { setCHAIN } = useCHAIN();
    const { setMODE } = useMODE();

    useEffect(() => {
        // highlight login button if username and password are not empty
        let loginButton = document.getElementById("Login");
        if (username !== '' && password !== '') {
            if (loginButton !== undefined) {
                loginButton.classList.add("formButtonSelected");
                loginButton.classList.remove("buttonForm");
            };
        } else {
            if (loginButton !== undefined) {
                loginButton.classList.remove("formButtonSelected");
                loginButton.classList.add("buttonForm");
            };
        };
    }, [username, password]);


    //////////////////
    // BACKEND API //
    /////////////////
    async function POSTdata(data) {
        const postURL = backendURL + data;

        try {
            const response = await fetch(postURL, {
              method: "GET",
            });
      
            if (response.status === 200) {
                const responseData = await response.json();
                console.log('Response Data:', responseData);

                // remove network error message
                setPostFail(false);

                let message = responseData.message;
                let MODE = responseData["MODE"];
                let store = responseData["STORE_ID"];
                let chain = responseData["CHAIN"];
                if (message === "Success") {
                    console.log("Login successful!");
                    sendLoggedin(true);
                    setMODE(MODE);
                    setCHAIN(chain);
                    if (store === "all") {
                        sendStoreSelect(true);
                    } else {
                        sendStoreSelect(false);
                        setStoreID(store);
                        sendState("Stock");
                    };
                    // reset username and password
                    setUsername('');
                    setPassword('');
                } else if (message === "Incorrect Username") {
                    setUsernameWarn(true);
                } else if (message === "Incorrect Password") {
                    setPasswordWarn(true);
                }
            } else {
                setPostFail(true);
            };
            
        } catch (error) {
            setPostFail(true);
            console.error('Error:', error);
        };
    };

    ///////////////////////////
    // FUNCTIONS FOR INPUTS //
    //////////////////////////
    // display username and password
    function displayUsername(event) {
        setUsername(event.target.value);
        // remove error message
        setUsernameWarn(false);
    };

    function displayPassword(event) {
        setPassword(event.target.value);
        // remove error message
        setPasswordWarn(false);
    };

    // reset user inputs
    function clearInputs(id) {
        let currInput = document.getElementById(id);
        currInput.classList.remove("inputError")
        let currInputValue = document.getElementById(id+'Input');
        currInputValue.placeholder = "";
    };

    ///////////////////////
    // FUNCTION: LOG IN //
    //////////////////////
    // function to send login data to backend
    function sendLogin() {
        if (username !== '' && password !== '') {
            // transform data into string
            let dataString = '?username=' + String(username) + '&password=' + String(password);
            // send POST request
            POSTdata(dataString);
        } else {
            // show warning if username and password are empty
            if (username === '' && password === '') {
                setUsernameWarn(true);
                setPasswordWarn(true);
            } else if (username === '') {
                setUsernameWarn(true);
            } else if (password === '') {
                setPasswordWarn(true);
            };
        };
        
    };


    return (
        <div className="div100">
            <div className="div30 userForm" >
                <img className="logoImage" alt="logo" src={logo} />
                <p className="textLarge textBlue spacing10">Admin</p>
                { postFail ? <p className="textUser textRed spacing2">Internal network issue please try again.</p> : null }

                <p className="text spacing">Username</p>
                { usernameWarn ? <p className="textUser textRed spacing2">Incorrect username</p> : null }
                <div id="Username" className="flexParent formFrame div50 spacingTop2 spacing5">
                    <div className="flex4">
                        <input id="UsernameInput" className="formInput formInputLogin textUser textDark textLeft"
                            onClick={() => clearInputs("Username")}
                            type="text"
                            value={username}
                            onChange={displayUsername}
                            placeholder=""
                        />
                    </div>
                    <div className="flex1"></div>
                </div>

                <p className="text spacing">Password</p>
                { passwordWarn ? <p className="textUser textRed spacing2">Incorrect password</p> : null }
                <div id="Password" className="flexParent formFrame div50 spacingTop2 spacing5">
                    <div className="flex4">
                        <input id="PasswordInput" className="formInput formInputLogin textUser textDark textLeft"
                            onClick={() => clearInputs("Password")}
                            type={ showPassword? "text" : "password" }
                            value={password}
                            onChange={displayPassword}
                            placeholder=""
                        />
                    </div>
                    <div className="flex1 centerVertical" onClick={() => setShowPassword(!showPassword)}>
                        <FontAwesomeIcon className="showIcon" icon={ showPassword ? faEyeSlash : faEye } />
                    </div>
                </div>

                <div className="spacing10" onClick={() => sendLogin()}>
                    <Link to="/login">
                        <button id="Login" className="div50 buttonForm textUser textBlue">Log In</button>
                    </Link>
                </div>

                {/* <div>
                    <p>
                        <span className="textUser textDark">Don't have an account?&emsp;</span>
                        <Link to={"/create-account"}>
                            <button className="div40 buttonForm textSmall textBlue spacingTop">Create Account</button>
                        </Link>
                    </p>
                </div> */}

            </div>
        </div>
    )
};