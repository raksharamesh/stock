import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faLock, faLockOpen  } from "@fortawesome/free-solid-svg-icons";
// import data
import configData from "../../data/config.json";
// import context
import { usePaddingTop, useStoreID, useCHAIN, useMODE } from '../Context';

export default function KioskControl() {
    const { storeID } = useStoreID();
    const { CHAIN } = useCHAIN();
    const { MODE } = useMODE();
    const { paddingTop } = usePaddingTop();
    // CONFIG
    const baseURL = configData[MODE]['APP_HOME'];
    const backendURL = baseURL + "management/";

    // STATE
    const [kioskData, setKioskData] = useState({});
    // flag
    const [postSuccess, setPostSuccess] = useState(false);


    // on load get kiosk data
    useEffect(() => {
        // transform data into string
        let dataString = '?store_number=' + String(storeID) + '&store_chain=' + String(CHAIN);
        // send POST request
        POSTdata('GET', 'portal_download_lock', dataString);
    }, []);


    //////////////////
    // BACKEND API //
    /////////////////
    async function POSTdata(state, address, data) {
        const postURL = backendURL + address + data;

        try {
            const response = await fetch(postURL, {
              method: "GET"
            });
      
            if (response.status === 200) {
                const responseData = await response.json();
                console.log('Response Data:', responseData);
 
                // set data depending on state
                if (state === 'GET') {
                    setKioskData(responseData);
                    // setShowStock(true);
                } else if (state === 'POST') {
                    let postRes = responseData.message
                    // setShowConfirm(false);
                    // show feedback for successful / fail update
                    if (postRes === 'Kiosk lock updated successfully!') {
                        // setShowStatus(true);
                        setPostSuccess(true);
                    } else {
                        // setShowStatus(true);
                        setPostSuccess(false);
                    };
                };
            };
            
        } catch (error) {
            console.error('Error:', error);
        };
    };

    ////////////////////////////////////
    // FUNCTION: LOCK / UNLOCK KIOSK //
    ///////////////////////////////////
    // function to send stock change to backend
    function sendChange(id, lock) {
        // convert status to string
        let newStatus;
        if (lock) {
            newStatus = "True";
        } else {
            newStatus = "False";
        };

        // transform data into string
        let dataString = '?kiosk_id=' + String(id) + '&store_chain=' + String(CHAIN) + '&locked=' + String(newStatus) + '&store_number=' + String(storeID);
        // send POST request
        POSTdata('POST', 'portal_update_lock', dataString).then(() => {
            if (postSuccess) {
                // update lock data
                let storeString = '?store_number=' + String(storeID) + '&store_chain=' + String(CHAIN);
                // send POST request
                POSTdata('GET', 'portal_download_lock', storeString);
            };
        });
    };


    let showKiosk = Object.keys(kioskData).map(k => {
        let name = kioskData[k]['Name'];
        let status = kioskData[k]['Locked'];

        let showStatus = status ? faLock : faLockOpen;
        let statusText = status ? "Unlock" : "Lock";
        let boxStyle = status ? "lockContainer div50 bgYellow" : "lockContainer div50";

        return (
            <div id={k} className="flex1">
                <p className="textTitle bold spacing3">{name}</p>
                <div className={boxStyle}  onClick={() => sendChange(k, !status)}>
                    <FontAwesomeIcon className="lockIcon spacing5" icon={showStatus} />
                    <p className="textMed bold">Click to {statusText} Kiosk</p>
                </div>
            </div>
        )
    });

    return (
        <div className="dashboardContainer" style={{ paddingTop: `${paddingTop}vh` }}>
            <div className="spacing2">
                <p className="textLarge bold textLeft">Kiosk Control</p>
                <p className="text textLeft">Toogle to lock and unlock kiosks</p>
            </div>
            <div className="flexParent spacingTop3">
                { showKiosk }
            </div>
        </div>
    )
};