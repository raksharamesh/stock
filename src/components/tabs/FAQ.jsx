import React, { useState, useEffect, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLayerGroup } from "@fortawesome/free-solid-svg-icons";
import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import faqData from "../../data/faq.json";
import lightbulbIcon from '../../image/icons/lightbulb.png';

// Dynamically import images
const importAll = (r) => r.keys().reduce((images, item) => {
    images[item.replace('./', '')] = r(item);
    return images;
}, {});
const images = importAll(require.context('../../image/faq', false, /\.(png|jpe?g|svg)$/));

// FAQCard Component
const FAQCard = ({ question, isOpen, onToggle }) => {
    const cardRef = useRef(null);

    useEffect(() => {
        if (isOpen && cardRef.current) {

            const tabPanelElement = document.querySelector('.react-tabs__tab-panel');
        
            if (tabPanelElement) {
                const computedStyle = window.getComputedStyle(tabPanelElement);
                const paddingTop = parseInt(computedStyle.paddingTop, 10);
                const scrollOffset = cardRef.current.getBoundingClientRect().top - paddingTop + 10;
                window.scrollBy({ top: scrollOffset, behavior: 'smooth' });
            }
        }
    }, [isOpen]);

    return (
        <div className="faqCard" ref={cardRef}>
            <div 
                className={`faqCardHeader ${isOpen ? 'faqCardHeaderOpen' : ''}`}
                onClick={onToggle}
            >
                <h3>{question['q-title']}</h3>
                <FontAwesomeIcon icon={isOpen ? faChevronUp : faChevronDown} style={{ paddingRight: '15px' }} />
            </div>
            {isOpen && (
                <div className="faqCardContent">
                    <ol>
                        {question.steps.map((step, stepIndex) => (
                            <li key={stepIndex}>
                                <h4>{step['s-title']}</h4>
                                <ul>
                                    {step.content.map((contentItem, contentIndex) => (
                                        <li key={contentIndex}>
                                            {contentItem.text && (
                                                <>
                                                    <img
                                                        src={lightbulbIcon}
                                                        alt="Light Icon"
                                                        style={{width: '16px', height: '16px', marginRight: '10px',marginTop: '5px'}}
                                                    />
                                                    {contentItem.text}
                                                </>
                                            )}
                                            {contentItem.image && (
                                                <img
                                                    src={images[contentItem.image]}
                                                    className={`faqImage faqImage${contentItem.size}`}
                                                    alt={contentItem.image}
                                                />
                                            )}
                                        </li>
                                    ))}
                                </ul>
                            </li>
                        ))}
                    </ol>
                </div>
            )}
        </div>
    );
};

// Main FAQ Component
export default function FAQ() {
    const [keyword, setKeyword] = useState('');
    const [filteredQuestions, setFilteredQuestions] = useState(faqData.questions);
    const [activeTabIndex, setActiveTabIndex] = useState(0);
    const [openCardIndex, setOpenCardIndex] = useState(null);

    useEffect(() => {
        filterQuestionsByTab();
    }, [activeTabIndex, keyword]);

    useEffect(() => {
        // Scroll to the top whenever the active tab changes
        window.scrollTo(0, 0);
        setOpenCardIndex(null);
    }, [activeTabIndex]);

    const filterQuestionsByTab = () => {
        let filtered = faqData.questions;

        if (keyword.trim() !== "") {
            const lowercasedKeyword = keyword.toLowerCase();
            filtered = filtered.filter((question) =>
                question['q-title'].toLowerCase().includes(lowercasedKeyword) ||
                question.steps.some(step =>
                    step['s-title'].toLowerCase().includes(lowercasedKeyword) ||
                    step.content.some(contentItem =>
                        contentItem.text && contentItem.text.toLowerCase().includes(lowercasedKeyword)
                    )
                )
            );
        }

        if (activeTabIndex !== 0) { 
            const category = Object.keys(faqData.category)[activeTabIndex - 1];
            const questionIds = faqData.category[category];
            filtered = filtered.filter(question => questionIds.includes(question.id));
        }

        setFilteredQuestions(filtered);
    };

    const handleKeywordChange = (event) => setKeyword(event.target.value);
    const handleClear = () => setKeyword('');
    const handleSearch = () => setActiveTabIndex(0);

    const searchBox = (
        <div className="flexParent searchBox">
            <div className="textFrame searchInputBox">
                <input
                    className="keywordInput textLeft"
                    type="text"
                    value={keyword}
                    onChange={handleKeywordChange}
                    placeholder="What problems are you experiencing?"
                    onKeyDown={(e) => e.key === 'Enter' && handleSearch()}
                />
                {keyword && <span className="clearButton" onClick={handleClear}>&times;</span>}
            </div>
            <button className="floatLeft buttonSearch" onClick={handleSearch}>Search</button>
        </div>
    );

    return (
        <div>
            <Tabs className="parentContainer" selectedIndex={activeTabIndex} onSelect={setActiveTabIndex}>
                <div id="fixedFAQ" className="fixedContainer">
                    <div className="searchBoxWrapper">{searchBox}</div>
                    <div className="tabListWrapper">
                        <TabList className="tabList">
                            <Tab><FontAwesomeIcon icon={faLayerGroup} style={{ marginRight: '5px', fontSize: '18px' }} />All</Tab>
                            {Object.keys(faqData.category).map((category, index) => (
                                <Tab key={index}><FontAwesomeIcon icon={faLayerGroup} style={{ marginRight: '5px', fontSize: '18px' }} />{category}</Tab>
                            ))}
                        </TabList>
                    </div>
                </div>
                <div id="scrollFAQ" className="scrollContainer">
                    <TabPanel>
                        {filteredQuestions.length > 0 ? (
                            filteredQuestions.map((question, index) => (
                                <FAQCard
                                    key={index}
                                    question={question}
                                    isOpen={openCardIndex === `${activeTabIndex}-${index}`}
                                    onToggle={() => setOpenCardIndex(openCardIndex === `${activeTabIndex}-${index}` ? null : `${activeTabIndex}-${index}`)}
                                />
                            ))
                        ) : (
                            <p className="noResultsMessage">No questions found.</p>
                        )}
                    </TabPanel>
                    {Object.entries(faqData.category).map(([category, questionIds], index) => (
                        <TabPanel key={index}>
                            {filteredQuestions.length > 0 ? (
                                filteredQuestions
                                    .filter(question => questionIds.includes(question.id))
                                    .map((question, questionIndex) => (
                                        <FAQCard
                                            key={questionIndex}
                                            question={question}
                                            isOpen={openCardIndex === `${activeTabIndex}-${questionIndex}`}
                                            onToggle={() => setOpenCardIndex(openCardIndex === `${activeTabIndex}-${questionIndex}` ? null : `${activeTabIndex}-${questionIndex}`)}
                                        />
                                    ))
                            ) : (
                                <p className="noResultsMessage">No questions found in this category.</p>
                            )}
                        </TabPanel>
                    ))}
                </div>
            </Tabs>
        </div>
    );
}
