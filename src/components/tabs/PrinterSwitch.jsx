import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faCircleCheck, faCircleXmark } from "@fortawesome/free-solid-svg-icons";
// import data
import configData from "../../data/config.json";

// static
// const MODE = configData['MODE']; // test or prod
// const baseURL = configData[MODE]['APP_HOME'];
// const backendURL = baseURL + "management/";


export default function PrinterSwitch({ MODE }) {
    // CONFIG (dynamic based on login credentials)
    const baseURL = configData[MODE]['APP_HOME'];
    const backendURL = baseURL + "management/";

    // STATE
    const [printerData, setPrinterData] = useState({});
    const [currentPrinter, setCurrentPrinter] = useState("");
    const [newPrinter, setNewPrinter] = useState("");
    const [newPrinterName, setNewPrinterName] = useState("");
    // flag
    const [showConfirm, setShowConfirm] = useState(false);
    const [showStatus, setShowStatus] = useState(false);
    const [postSuccess, setPostSuccess] = useState(false);


    // get printer data on component mount
    useEffect(() => {
        POSTdata("GET", "kitchen_printer/printer_data", "");
    }, []);

    // define timer
    let timer;
    // define timer for status feedback
    React.useEffect(() => {
        if (showStatus) {
            clearTimeout(timer);
            // set longer timer if post is successful
            if (postSuccess) {
                timer = setTimeout(setShowStatus, 1000, false);
            } else {
                timer = setTimeout(setShowStatus, 5000, false);
            };
        } else {
            clearTimeout(timer);
        };
        return () => clearTimeout(timer);
    }, [showStatus]);



    //////////////////
    // BACKEND API //
    /////////////////
    async function POSTdata(state, address, data) {
        const postURL = backendURL + address + data;

        try {
            const response = await fetch(postURL, {
              method: "GET"
            });
      
            if (response.status === 200) {
                const responseData = await response.json();
                console.log('Response Data:', responseData);
 
                // set data depending on state
                if (state === 'GET') {
                    setPrinterData(responseData);
                    setCurrentPrinter(responseData["PRINTER_NAME"]);
                } else if (state === 'POST') {
                    let postRes = responseData.message
                    // setShowConfirm(false);
                    // show feedback for successful / fail update
                    if (postRes === 'Printer switch successful!') {
                        // update buttons by pulling data again 
                        POSTdata("GET", "kitchen_printer/printer_data", "");
                        setShowStatus(false);
                        setPostSuccess(true);
                    } else {
                        setShowStatus(true);
                        setPostSuccess(false);
                    };
                };
            };
            
        } catch (error) {
            console.error('Error:', error);
        };
    };


    //////////////////////
    // PRINTER BUTTONS //
    /////////////////////
    // function to ask confirm
    function askConfirmSwitch(printer) {
        setShowConfirm(true);
        setNewPrinter(printer);
        setNewPrinterName(printerData[printer]["Name"]);
    };

    // create printer buttons
    let printerButtons = Object.keys(printerData).map((k) => {
        let name = printerData[k]["Name"];
        let state = "Inactive"

        if (k === "PRINTER_NAME") {
            return; 
        } else {

            if (currentPrinter === k) {
                state = "Active";
                return (
                    <div className="flex1 spacingTop5">
                        <button className="buttonBlank buttonLarge bgOrange">
                            <p className="textMed bold textWhite">{name}</p>
                            <p className="textMed textWhite">{state}</p>
                        </button>
                    </div>
                )
            } else {
                // only allow clicks if it is not the currently active printer
                return (
                    <div className="flex1 spacingTop5" onClick={() => askConfirmSwitch(k)}>
                        <button className="buttonBlank buttonLarge">
                            <p className="textMed bold textWhite">{name}</p>
                            <p className="textMed textWhite">{state}</p>
                        </button>
                    </div>
                )
            };
        };
        
    });


    ////////////////////
    // CONFIRM POPUP //
    ///////////////////
    // function to send printer switch request
    function switchPrinter() {
        setShowConfirm(false);
        let data = "?printer_name=" + newPrinter;
        POSTdata("POST", "kitchen_printer/switch_printer", data);
    };

    let confirmPopup = <div className="popupVeil">
                            <div className="popupContainer div30">
                                <div className="spacingTop5 spacing5">
                                    <p className="text spacing2">
                                        <span>Are you sure you want to switch to the </span>
                                        <span className="bold">{newPrinterName}</span>
                                        <span>?</span>
                                    </p>
                                </div>
                                <div className="flexParent">
                                    <div className="flex1">
                                        <button className="button buttonBlank buttonCancel textMed bold" onClick={() => setShowConfirm(false)}>Cancel</button>
                                    </div>
                                    <div className="flex1">
                                        <button className="button buttonBlank buttonConfirm textMed bold" onClick={() => switchPrinter()}>Confirm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

    /////////////////////////
    // UPDATE STATUS ICON //
    ////////////////////////
    let updateSuccess = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                            <div className="statusContainer">
                                <FontAwesomeIcon className="successIcon spacing2" icon={faCircleCheck} />
                                <p className="textMed bold">Recieved Switch Printer Request</p>
                            </div>
                        </div>
   
    let updateFail = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                        <div className="statusContainer">
                            <FontAwesomeIcon className="failIcon spacing2" icon={faCircleXmark} />
                            <p className="textMed bold">Error Processing Request</p>
                            <p className="textMed bold">Please Try Again.</p>
                        </div>
                    </div>

    let statusUpdate = postSuccess ? updateSuccess : updateFail;

    return (
        <>
            { showStatus ? statusUpdate : null }
            { showConfirm ? confirmPopup : null }
            <div className="spacingTop10 div40 printAllContainer">
                <div className="spacing2">
                    <p className="textLarge bold textLeft">Switch Printer</p>
                    <p className="text textLeft">Choose the printer you want to switch to.</p>
                </div>

                <div className="flexParent">
                    { printerButtons }
                </div>
            </div>
        </>
    )

};