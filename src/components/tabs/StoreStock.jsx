import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faCircleCheck, faCircleXmark, faPen } from "@fortawesome/free-solid-svg-icons";
// import data
import configData from "../../data/config.json";
import storeList from "../../data/stores.json";
// import context
import { usePaddingTop, useStoreID, useCHAIN, useMODE } from '../Context';


export default function StoreStock() {
    const { storeID } = useStoreID();
    const { CHAIN } = useCHAIN();
    const { MODE } = useMODE();
    const { paddingTop } = usePaddingTop();
    
    // CONFIG (dynamic based on login credentials)
    const baseURL = configData[MODE]['APP_HOME'];
    const backendURL = baseURL + "management/";

    // STATE
    const [changeID, setChangeID] = useState('');
    const [changeStat, setChangeStat] = useState('');
    const [stockData, setStockData] = useState({});
    // flag
    const [showConfirm, setShowConfirm] = useState(false);
    const [showStatus, setShowStatus] = useState(false);
    const [postSuccess, setPostSuccess] = useState(false);


    // on load get stock data
    useEffect(() => {
        // transform data into string
        let dataString = '?store_number=' + String(storeID) + '&store_chain=' + String(CHAIN);
        // send POST request
        POSTdata('GET', 'portal_download_stock', dataString);
    }, []);

    // refresh data when status is confirmed (allow immediete user feedback)
    useEffect(() => {
        // transform data into string
        let dataString = '?store_number=' + String(storeID) + '&store_chain=' + String(CHAIN);
        // send POST request
        POSTdata('GET', 'portal_download_stock', dataString);
    }, [showConfirm]);


    // define timer
    let timer;
    // define timer for status feedback
    React.useEffect(() => {
        if (showStatus) {
            clearTimeout(timer);
            // set longer timer if post is successful
            if (postSuccess) {
                timer = setTimeout(setShowStatus, 750, false);
            } else {
                timer = setTimeout(setShowStatus, 2000, false);
            };
        } else {
            clearTimeout(timer);
        };
        return () => clearTimeout(timer);
    }, [showStatus]);


    //////////////////
    // BACKEND API //
    /////////////////
    async function POSTdata(state, address, data) {
        const postURL = backendURL + address + data;

        try {
            const response = await fetch(postURL, {
              method: "GET"
            });
      
            if (response.status === 200) {
                const responseData = await response.json();
                console.log('Response Data:', responseData);
 
                // set data depending on state
                if (state === 'GET') {
                    setStockData(responseData);
                    // setShowStock(true);
                } else if (state === 'POST') {
                    let postRes = responseData.message
                    setShowConfirm(false);
                    // show feedback for successful / fail update
                    if (postRes === 'Stock updated successfully!') {
                        setShowStatus(true);
                        setPostSuccess(true);
                    } else {
                        setShowStatus(true);
                        setPostSuccess(false);
                    };
                };
            };
            
        } catch (error) {
            console.error('Error:', error);
        };
    };


    ///////////////////
    // CHANGE POPUP //
    //////////////////
    // function to send stock change to backend
    function sendChange() {
        // convert status to boolean
        let newStatus;
        if (changeStat === 'In Stock') {
            newStatus = 'True'
        } else if (changeStat === 'Out of Stock') {
            newStatus = 'False'
        };

        // transform data into string
        let dataString = '?item_id=' + String(changeID) + '&in_stock=' + String(newStatus) + '&store_number=' + String(storeID) + '&store_chain=' + String(CHAIN);
        // send POST request
        POSTdata('POST', 'portal_update_stock', dataString).then(() => {
            if (postSuccess) {
                // update stock data
                let storeString = '?store_number=' + String(storeID) + '&store_chain=' + String(CHAIN);
                // send POST request
                POSTdata('GET', 'portal_download_stock', storeString);
            };
        });
    };

    // create popup
    let confirmPopup = <div className="popupVeil">
                            <div className="popupContainer div30">
                                {/* <FontAwesomeIcon className="closeIcon" icon={ faCircleXmark} onClick={() => setShowConfirm(false)} /> */}
                                <div className="spacingTop5 spacing5">
                                    <p className="text spacing2">Are you sure that item {changeID}</p>
                                    <p className="text bold">{ showConfirm ? stockData[changeID]["Item En"] : null }</p>
                                    <p className="text bold spacing2">{ showConfirm ? stockData[changeID]["Item Zh"] : null }</p>
                                    <p className="text">
                                        <span>is </span>
                                        <span className="textMed bold textRed">{changeStat}</span>
                                    </p>
                                </div>
                                <div className="flexParent">
                                    <div className="flex1">
                                        <button className="button buttonBlank buttonCancel textMed bold" onClick={() => setShowConfirm(false)}>Cancel</button>
                                    </div>
                                    <div className="flex1">
                                        <button className="button buttonBlank buttonConfirm textMed bold" onClick={() => sendChange()}>Confirm</button>
                                    </div>
                                </div>
                            </div>
                        </div>


    /////////////////////////
    // UPDATE STATUS ICON //
    ////////////////////////
    let updateSuccess = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                            <div className="statusContainer">
                                <FontAwesomeIcon className="successIcon spacing2" icon={faCircleCheck} />
                                <p className="textMed bold">Update Sucessful</p>
                            </div>
                        </div>
    
    let updateFail = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                        <div className="statusContainer">
                            <FontAwesomeIcon className="failIcon spacing2" icon={faCircleXmark} />
                            <p className="textMed bold">Update Unsuccessful</p>
                            <p className="textMed bold">Please Try Again</p>
                        </div>
                    </div>

    let statusUpdate = postSuccess ? updateSuccess : updateFail;

    ///////////////////
    // CREATE TABLE //
    //////////////////
    // function to set stock status
    function changeStatus(e) {
        let idStat = e.currentTarget.id.split("|");
        let id = idStat[0];
        let status = idStat[1];

        // set confirm popup and fill data
        setShowConfirm(true);
        setChangeID(id);
        if (status === 'In Stock') {
            setChangeStat('Out of Stock');
        } else if (status === 'Out of Stock') {
            setChangeStat('In Stock');
        };
    };

    // create in stock and out of stock icons
    const inStock = <div className="flexParent">
                    <div className="flex1">
                        <FontAwesomeIcon className="checkedIcon" icon={ faCircleCheck } />
                    </div>
                    <div className="flex3">
                        <p className="text textLeft">In Stock</p>
                    </div>
                </div>

    const outOfStock = <div className="flexParent">
                        <div className="flex1">
                            <FontAwesomeIcon className="crossedIcon" icon={ faCircleXmark } />
                        </div>
                        <div className="flex3">
                            <p className="text textLeft">Out of Stock</p>
                        </div>
                    </div>

    // stock table header
    let stockHeader = <div className="flexParent tableHeader">
                        <div className="flex1">
                            <p className="textMed bold textWhite">ID</p>
                        </div>
                        <div className="flex3">
                            <p className="textMed textLeft bold textWhite">English Name</p>
                        </div>
                        <div className="flex3">
                            <p className="textMed textLeft bold textWhite">Chinese Name</p>
                        </div>
                        <div className="flex2">
                            <p className="textMed textLeft bold textWhite">Stock Status</p>
                        </div>
                    </div>


    // create stock table
    let stockTable = Object.keys(stockData).map((k, i) => {
        // extract data
        let nameEn = stockData[k]['Item En'];
        let nameZh = stockData[k]['Item Zh'];
        let altStriping = i % 2 === 0 ? "flexParent tableRows" : "flexParent tableRows stripeBlue";
        let stockStatus = stockData[k]["Stock"] ? inStock : outOfStock;
        let idStatus = stockData[k]["Stock"] ? k + "|In Stock" : k + "|Out of Stock"; 

        return (
            <div id={idStatus} className={altStriping} onClick={(e) => changeStatus(e)}>
                <div className="flex1">
                    <p className="text bold">{k}</p>
                </div>
                <div className="flex3">
                    <p className="text textLeft">{nameEn}</p>
                </div>
                <div className="flex3">
                    <p className="text textLeft">{nameZh}</p>
                </div>
                <div className="flex2">
                    { stockStatus }
                </div>
            </div>
        )
        
    });

    // create stock content
    let stockContent = <>
                            <div className="flexParent spacing2">
                                <div className="flex3">
                                    <p className="textLarge bold textLeft">What's in Stock at {storeList[CHAIN][storeID]}</p>
                                    <p className="text textLeft">Click on a row to change the stock status</p>
                                </div>
                                <div className="flex1">
                                </div>
                            </div>
                                
                            <div className="stockTable">
                                { stockHeader }
                                { stockTable }
                            </div>
                        </>

    
    return (
        <>
            { showConfirm ? confirmPopup : null }
            { showStatus ? statusUpdate : null }
            <div className="dashboardContainer" style={{ paddingTop: `${paddingTop}vh` }}>
                { stockContent }
            </div>
        </>
    )
}