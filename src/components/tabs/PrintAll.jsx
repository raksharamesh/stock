import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faCircleCheck, faCircleXmark, faPrint } from "@fortawesome/free-solid-svg-icons";
// import data
import configData from "../../data/config.json";

export default function PrintAll({ MODE }) {
    // CONFIG (dynamic based on login credentials)
    const baseURL = configData[MODE]['APP_HOME'];
    const backendURL = baseURL + "management/";
    const printerName = "front_kitchen";

    // flag
    const [postSuccess, setPostSuccess] = useState(false);
    const [showStatus, setShowStatus] = useState(false);

    // define timer
    let timer;
    // define timer for status feedback
    React.useEffect(() => {
        if (showStatus) {
            clearTimeout(timer);
            // set longer timer if post is successful
            if (postSuccess) {
                timer = setTimeout(setShowStatus, 1000, false);
            } else {
                timer = setTimeout(setShowStatus, 5000, false);
            };
        } else {
            clearTimeout(timer);
        };
        return () => clearTimeout(timer);
    }, [showStatus]);


    ///////////////////////////////////////
    // FUNCTION: SEND PRINT API REQUEST //
    //////////////////////////////////////
    // function to request print all 
    async function POSTprint() {
        let data = "?printer_name=" + printerName;
        let postURL = backendURL + "kitchen_printer/print_all" + data;

        try {
            const response = await fetch(postURL, {
                method: "GET"
            });

            // user feedback
            setShowStatus(true);

            if (response.status === 200) {
                setPostSuccess(true);
            } else {
                setPostSuccess(false);
            };
        } catch (error) {
            console.error('Error:', error);
        };
    };

    /////////////////////////
    // UPDATE STATUS ICON //
    ////////////////////////
    let updateSuccess = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                            <div className="statusContainer">
                                <FontAwesomeIcon className="successIcon spacing2" icon={faCircleCheck} />
                                <p className="textMed bold">Recieved Print All Request</p>
                            </div>
                        </div>
   
    let updateFail = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                        <div className="statusContainer">
                            <FontAwesomeIcon className="failIcon spacing2" icon={faCircleXmark} />
                            <p className="textMed bold">Error Processing Request</p>
                            <p className="textMed bold">Please Try Again.</p>
                        </div>
                    </div>

    let statusUpdate = postSuccess ? updateSuccess : updateFail;


    ///////////////////////////////
    // CREATE SEND PRINT BUTTON //
    //////////////////////////////
    let buttonStyle = showStatus ? "floatLeft buttonBlank buttonBig bgYellow" : "floatLeft buttonBlank buttonBig";
    let buttonTextStyle = showStatus ? "textMed textDark" : "textMed textWhite";
    let printButton = <div className="spacingTop5" onClick={() => POSTprint()}>
                            <button className={buttonStyle}>
                                <p className={buttonTextStyle}>Resume Printing</p>
                            </button>
                        </div>

    return (
        <>
            { showStatus ? statusUpdate : null }
            <div className="spacingTop10 div40 printAllContainer floatLeft">
                <div className="spacing2">
                    <p className="textLarge bold textLeft">Troubleshoot</p>
                    <p className="text textLeft">Press the button below to resume all print jobs.</p>
                </div>

                { printButton }
            </div>
        </>
    )
};