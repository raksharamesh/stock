import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faCircleCheck, faCircleXmark, faMoneyBill1Wave } from "@fortawesome/free-solid-svg-icons";
// import config
import configData from '../../data/config.json';
import { usePaddingTop, useStoreID, useCHAIN, useMODE } from '../Context';


export default function CashOrders() {
    const { storeID } = useStoreID(); 
    const { CHAIN } = useCHAIN();
    const { MODE } = useMODE();
    const { paddingTop } = usePaddingTop();

    // CONFIG (dynamic based on login credentials)
    const baseURL = configData[MODE]['APP_HOME'];
    // const targetURL = baseURL + "square";
    // parameters for cash API calls
    const endpoint = configData[MODE][CHAIN]["BACKEND_CASH"];
    const cashURL = baseURL + endpoint + "retrieve_order"; // 'square/cashorder/retrieve_order'
    const paidURL = baseURL + endpoint + "process_payment"; // 'square/cashorder/process_payment'
    const locationID = configData[MODE][CHAIN]['LOCATION_ID'][storeID]; // location ID is different for each store
    const merchantID = configData[MODE][CHAIN]['MERCHANT_ID'][storeID];

    // STATE
    const [inputOrderNum, setInputOrderNum] = useState('');
    const [amountPaid, setAmountPaid] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [orderNum, setOrderNum] = useState('');
    const [cashOwed, setCashOwed] = useState('');
    const [orderDetails, setOrderDetails] = useState({});
    const [cashChange, setCashChange] = useState('');
    const [inputErrorMessage, setInputErrorMessage] = useState('');
    // flag
    const [showPayment, setShowPayment] = useState(false);
    const [showStatus, setShowStatus] = useState(false);
    const [postSuccess, setPostSuccess] = useState(false);
    const [clickConfirm, setClickConfirm] = useState(false);
    const [inputWarning, setInputWarning] = useState(false);


    // define timer
    let timer;
    // define timer for status feedback
    useEffect(() => {
        if (showStatus) {
            clearTimeout(timer);
            // set longer timer if post is successful
            if (postSuccess) {
                timer = setTimeout(setShowStatus, 1000, false);
            } else {
                timer = setTimeout(setShowStatus, 5000, false);
            };
        } else {
            clearTimeout(timer);
        };
        return () => clearTimeout(timer);
    }, [showStatus]);

    // define timer
    let timerPopup;
    // define timer payment popup
    useEffect(() => {
        if (showPayment) {
            clearTimeout(timerPopup);
            // start time out countdown after confirm button is clicked (1 min)
            if (clickConfirm) {
                timerPopup = setTimeout(function() {
                    setShowPayment(false);
                    setClickConfirm(false);
                }, 60000);
            };
        } else {
            clearTimeout(timerPopup);
        };
        return () => clearTimeout(timerPopup);
    }, [showPayment, clickConfirm]);


    //////////////////////////
    // TEXT INPUT FUNCTION //
    ////////////////////////
    // function to display and save order number in text input
    function displayOrderNum(event) {
        setInputOrderNum(event.target.value);
    };

    // function to display amount
    function displayAmountPaid(event) {
        // setAmountPaid(event.target.value);
        let input = event.target.value;

        // restrict input to numbers only
        if (/^-?\d*(\.\d*)?$/.test(input)) {
            setAmountPaid(event.target.value);
        };
    };

    /////////////////////////////////
    // FUNTION: POST ORDER DETAILS //
    ////////////////////////////////
    async function POSTOrder() {
        // format response data
        let requestData = JSON.stringify({
            "merchant_id": merchantID,
            "location_id": locationID,
            "order_num": inputOrderNum
        });
        // console.log('POST Order Data:', requestData);

        try {
            const response = await fetch(cashURL, {
                method: "POST",
                headers: { 
                    'Content-Type': 'application/json',
                },
                body: requestData
            });

      
            if (response.status === 200) {
                const responseData = await response.json();
                // save data
                setOrderNum(responseData['order_num']);
                setCashOwed(responseData['payment_amt'] / 100);
                setOrderDetails(responseData['order_details']);
                // console.log('POST Order Response Data:', responseData);

                // open popup reset input field and change field
                setShowPayment(true);
                setCashChange('');
                setAmountPaid('');
                setInputWarning(false);
                setInputErrorMessage('');
                // clear input field
                setInputOrderNum('');
            } else if (response.status === 404) {
                setShowStatus(true);
                setPostSuccess(false);
                setErrorMessage(['Order Not Found', 'Please Try a Different Order Number']);
            } else if (response.status === 500) {
                setShowStatus(true);
                setPostSuccess(false);
                setErrorMessage(['Server Error', 'Please Try Again Later']);
            };
            
        } catch (error) {
            setShowStatus(true);
            setPostSuccess(false);
            setErrorMessage(['Server Error', 'Please Try Again Later']);
            console.error('Error POSTorder:', error);
        };
    };

    ////////////////////////////////////////////
    // FUNCTION: POST CASH PAID CONFIRMATION //
    ///////////////////////////////////////////
    async function POSTpaid() {
        // set clicked
        setClickConfirm(true);

        // format response data
        let change = (Math.round( (amountPaid - cashOwed) * 100 ) / 100).toFixed(2);
        let paid = parseFloat(amountPaid).toFixed(2);
        let displayChange = "Change: $" + change;
        setCashChange(displayChange);

        let cashData = JSON.stringify({
            "merchant_id": merchantID,
            "location_id": locationID,
            "order_num": orderNum,
            "amount_owed": cashOwed,
            "cash_tendered": paid,
            "cash_change": change,
        });
        // console.log('POST Cash Data:', cashData);

        try {
            const response = await fetch(paidURL, {
                method: "POST",
                headers: { 
                    'Content-Type': 'application/json',
                },
                body: cashData
            });

            // show status
            setShowStatus(true);
      
            if (response.status === 200) {
                const responseData = await response.json();
                // console.log('POST Response Data:', responseData);

                setPostSuccess(true);
                // close popup and clear input field
                // setShowPayment(false);
                // clear input field
                setAmountPaid('');

            } else if (response.status === 409) {
                setPostSuccess(false);
                setErrorMessage(['Order Already Paid For', '']);
            } else if (response.status === 422) {
                setPostSuccess(false);
                setErrorMessage(['Amount Paid is Less then Amount Due', 'Please Enter a Valid Amount']);
            } else {
                setPostSuccess(false);
                setErrorMessage(['Unable to Create Order', 'Please Try Again']);
            };
            
        } catch (error) {
            setPostSuccess(false);
            setErrorMessage(['Unable to Create Order', 'Please Try Again']);
            console.error('Error POSTpaid:', error);
        };
    };


    /////////////////////////
    // UPDATE STATUS ICON //
    ////////////////////////
    let updateSuccess = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                            <div className="statusContainer">
                                <FontAwesomeIcon className="successIcon spacing2" icon={faCircleCheck} />
                                <p className="textMed bold">Cash Order Processed Successfully</p>
                            </div>
                        </div>
   
    let updateFail = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                        <div className="statusContainer">
                            <FontAwesomeIcon className="failIcon spacing2" icon={faCircleXmark} />
                            <p className="textMed bold">{errorMessage[0]}</p>
                            <p className="textMed bold">{errorMessage[1]}</p>
                        </div>
                    </div>

    let statusUpdate = postSuccess ? updateSuccess : updateFail;

    ///////////////////
    // CREATE POPUP //
    //////////////////
    // function to display error message for input errors
    function displayInputErrorMsg(msg) {
        setInputWarning(true);
        setInputErrorMessage(msg);
    };

    let doneButton;
    if (amountPaid === '') {
        doneButton = <div className="spacingTop5" onClick={() => displayInputErrorMsg("Please Enter an Amount")}>
                        <button className="buttonBlank buttonConfirmSmall">
                            <p className="text">Done</p>
                        </button>
                    </div>
    } else if (amountPaid < cashOwed) {
        doneButton = <div className="spacingTop5" onClick={() => displayInputErrorMsg("Please Enter a Valid Amount")}>
                        <button className="buttonBlank buttonConfirmSmall">
                            <p className="text">Done</p>
                        </button>
                    </div>
    } else {
        doneButton = <div className="spacingTop5" onClick={() => POSTpaid()}>
                        <button className="buttonBlank buttonConfirmSmall bgYellow">
                            <p className="text">Done</p>
                        </button>
                    </div>
    };


    // create amount paid popup
    let paymentPopup = <div className="popupVeil">
                            <div className="popupContainer">
                                <FontAwesomeIcon className="closeIcon" icon={faCircleXmark} onClick={() => setShowPayment(false)}/>
                                
                                <div className="spacingTop5 div80">
                                    <p className="textLarge spacing2">
                                        <span>Cash Order </span>
                                        <span className="bold">#{orderNum}</span>
                                    </p>
                                    <p className="textLarge bold spacing5">
                                        <span>Amount Due: </span>
                                        <span className="textRed">${cashOwed}</span>
                                    </p>
                                    <p className="text textLeft spacingTop2">
                                        <span>Enter </span>
                                        <span className="bold">amount customer is paying</span>
                                    </p>
                                    { inputWarning ? <p className="textLarge bold textRed spacingTop5">{inputErrorMessage}</p> : null }
                                    <div className="flexParent textFrame spacingTop5 spacing5" onClick={() => setInputWarning(false)}>
                                        <div className="flex1 textRight">
                                            <label for="amount" className="textUser textGrey">$</label>
                                        </div>
                                        <div className="flex8">
                                            <input id="" className="formInput menuUpdateInput textUser textDark textLeft"
                                                name="amount"
                                                type="text"
                                                value={amountPaid}
                                                onChange={displayAmountPaid}
                                                placeholder="">
                                            </input>
                                        </div>
                                    </div> 

                                    <p className="textLarge bold textRed spacing2">{cashChange}</p>

                                    { doneButton }                 

                                    {/* <div className="spacingTop5" onClick={() => POSTpaid()}>
                                        <button className="buttonBlank buttonConfirmSmall">
                                            <p className="text">Done</p>
                                        </button>
                                    </div> */}

                                </div>
                                
                            </div>
                        </div>


    /////////////////////////
    // CREATE ORDER INPUT //
    ///////////////////////
    let orderInput = <div className="dashboardContainer" style={{ paddingTop: `${paddingTop}vh` }}>
                        <div className="spacing2">
                            <p className="textLarge bold textLeft">Got a Cash Order?</p>
                            <p className="text textLeft">Please enter the order number found on the customer's receipt.</p>
                        </div>

                        <div className="flexParent">
                            <FontAwesomeIcon className="floatLeft menuIcon" icon={faMoneyBill1Wave} />
                            <div className="flex1 paddingLeft2">
                                <div className="div80 floatLeft textFrame">
                                    <input id="" className="formInput menuUpdateInput textUser textDark textLeft"
                                        type="text"
                                        value={inputOrderNum}
                                        onChange={displayOrderNum}
                                        placeholder="Order Number">
                                    </input>
                                </div>
                            </div>
                            
                            <div className="flex2" onClick={() => POSTOrder()}>
                                <button className="floatLeft buttonBlank buttonConfirmReprint">
                                    <p className="text">Confirm</p>
                                </button>
                            </div>
                        </div>
                    </div>


    return (
        <>
            { showPayment ? paymentPopup : null }
            { showStatus ? statusUpdate : null }
            { orderInput }
        </>
    )
};