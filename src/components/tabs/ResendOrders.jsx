import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import icons
import { faCircleCheck, faCircleXmark, faTabletScreenButton } from "@fortawesome/free-solid-svg-icons";
// import config
import configData from '../../data/config.json';
// import context
import { usePaddingTop, useStoreID, useCHAIN, useMODE } from '../Context';


export default function ResendOrders() {
    const { storeID } = useStoreID(); 
    const { CHAIN } = useCHAIN();
    const { MODE } = useMODE();
    const { paddingTop } = usePaddingTop();

    // CONFIG (dynamic based on login credentials)
    const locationID = configData[MODE][CHAIN]['LOCATION_ID'][storeID] + '/'; // location ID is different for each store
    const baseURL = configData[MODE]['APP_HOME'];
    const endpoint = configData[MODE][CHAIN]["BACKEND_KDS"];
    const backendURL = baseURL + endpoint + locationID;

    // STATE
    const [orderNum, setOrderNum] = useState('');
    const [requestStatus, setRequestStatus] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    // flag
    const [showStatus, setShowStatus] = useState(false);
    const [postSuccess, setPostSuccess] = useState(false);


    // define timer
    let timer;
    // define timer for status feedback
    React.useEffect(() => {
        if (showStatus) {
            clearTimeout(timer);
            // set longer timer if post is successful
            if (postSuccess) {
                timer = setTimeout(setShowStatus, 1000, false);
            } else {
                timer = setTimeout(setShowStatus, 5000, false);
            };
        } else {
            clearTimeout(timer);
        };
        return () => clearTimeout(timer);
    }, [showStatus]);

    // define error message
    React.useEffect(() => {
        if (requestStatus === 404) {
            setErrorMessage(['Order Not Found', 'Please Try a Different Order Number']);
        } else {
            setErrorMessage(['Unsuccessful', 'Please Try Again']);
        };
    }, [requestStatus]);


    // function to display and save order number in text input
    function displayOrderNum(event) {
        setOrderNum(event.target.value);
    };

    //////////////////
    // BACKEND API //
    /////////////////
    async function POSTdata(data) {
        const postURL = backendURL + data;

        try {
            const response = await fetch(postURL, {
              method: "GET",
            });

            // show status popup (user feedback)
            setShowStatus(true);
            setRequestStatus(response.status);
      
            if (response.status === 200) {
                const responseData = await response.json();
                console.log('Response Data:', responseData);
                
                setPostSuccess(true);
                // clear input field
                setOrderNum('');
            } else {
                setPostSuccess(false);
            };
            
        } catch (error) {
            setPostSuccess(false);
            console.error('Error:', error);
        };
    };

    /////////////////////////
    // UPDATE STATUS ICON //
    ////////////////////////
    let updateSuccess = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                            <div className="statusContainer">
                                <FontAwesomeIcon className="successIcon spacing2" icon={faCircleCheck} />
                                <p className="textMed bold">Success</p>
                            </div>
                        </div>
    
    let updateFail = <div className="popupVeil" onClick={() => setShowStatus(false)}>
                        <div className="statusContainer">
                            <FontAwesomeIcon className="failIcon spacing2" icon={faCircleXmark} />
                            <p className="textMed bold">{errorMessage[0]}</p>
                            <p className="textMed bold">{errorMessage[1]}</p>
                        </div>
                    </div>

    let statusUpdate = postSuccess ? updateSuccess : updateFail;


    return (
        <>
            { showStatus ? statusUpdate : null }
            <div className="dashboardContainer" style={{ paddingTop: `${paddingTop}vh` }}>
                <div className="spacing2">
                    <p className="textLarge bold textLeft">Send an Order to the Kitchen Display</p>
                    <p className="text textLeft">Please enter an order number or customer name to be sent to the Kitchen Display.</p>
                </div>

                <div className="flexParent">
                    <FontAwesomeIcon className="floatLeft menuIcon" icon={faTabletScreenButton} />
                    <div className="flex1 paddingLeft2">
                        <div className="div80 floatLeft textFrame">
                            <input id="" className="formInput menuUpdateInput textUser textDark textLeft"
                                type="text"
                                value={orderNum}
                                onChange={displayOrderNum}
                                placeholder="Order Number / Customer Name">
                            </input>
                        </div>
                    </div>
                    
                    <div className="flex2" onClick={() => POSTdata(orderNum)}>
                        <button className="floatLeft buttonBlank buttonConfirmReprint">
                            <p className="text">Confirm</p>
                        </button>
                    </div>
                </div>
            </div>
        </>
    )

};