// import config
import configData from '../data/config.json';

export async function GETwebsocketStatus(CHAIN, MODE) {
    if (!navigator.onLine) {
        console.log("No internet connection, unable to get websocket status");
        return;
    };

    // if undefined do not send request
    if (CHAIN && MODE) {
        let baseURL = configData[MODE]["APP_HOME"];
        let endpoint = configData[MODE][CHAIN]["WEBSOCKET_STATUS"];
        const postURL = baseURL + endpoint;

        try {
            const response = await fetch(postURL, {
                method: "GET"
            });
    
            if (response.status === 200) {
                const responseData = await response.json();
                return responseData["Active clients"];
            };
            
        } catch (error) {
            console.error("Error fetching websocket status: ", error);
            return;
        };
    } else {
        return;
    };
};