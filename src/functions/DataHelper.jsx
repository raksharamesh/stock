export function doesValueExist(obj, value) {
    if (obj) {
        return Object.values(obj).includes(value);
    } else {
        console.log("Data in doesValueExist is undefined");
        return true;
    };
};