# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

## FAQ Page

### Description
The FAQ page is designed to provide users with answers to common questions through a structured and easily navigable interface. The content displayed on this page is managed via the `faq.json` file, which organizes questions into categories and outlines the steps necessary to resolve each issue. Images associated with these steps are stored separately in the `/src/image/faq` directory.

### Updating FAQ Content
This is the represenetation of faq.json:

```plaintext
faq.json/
│
├── category                    # Mapping of categories to question IDs
│   ├── "Label Printer": [1, 2, 10]
│   ├── "Device": [3, 4]
│   ├── "Order Display": [5, 6]
│   ├── "Kiosk Payment": [7, 8]
│   └── "Other": [9, 10]
│
└── questions                   # Array of question objects
    ├── {
    │     "id": 1,
    │     "q-title": "Label Sticker is not printing.",
    │     "steps": [
    │         {
    │             "s-title": "Turn on the printer...",
    │             "content": [
    │                 {"text": "If it's powered on..."},
    │                 {"image": "Test1.png", "size": "Medium"}
    │             ]
    │         },
    |         {
    │             "s-title": "Replace label paper roll...",
    │             "content": []
    │         },
    │         ...
    │     ]
    │   },
    ├── {
    │     "id": 2,
    │     "q-title": "Label stickers are printed in the wrong format.",
    │     "steps": [
    │         {
    │             "s-title": "Check if the label printer...",
    │             "content": [
    │                 {"text": "Refer to the calibration guide..."}
    │             ]
    │         },
    │         ...
    │     ]
    │   },
    │   ...
    └── {
          "id": 10,
          "q-title": "How to replace the receipt paper ...",
          "steps": [
              {
                  "s-title": "Open the printer cover in the kiosk.",
                  "content": [
                      {"text": "Ensure the kiosk is powered off..."},
                      {"text": "After ........."},
                      {"image": "Test2.png", "size": "Large"}

                  ]
              },
              ...
          ]
        }
```

#### Text
- Location: `src/data/faq.json`
- The `faq.json` file is structured into two main sections:
    - **category**: Defines the mapping of categories to question IDs.
    - **questions**: Contains a list of questions and their corresponding steps.

##### (1) Category
- Category Name (**key**): The key represents the category name (e.g., "Label Printer", "Device").
- Question IDs (**value**): The array of integers lists the IDs of the questions that belong to this category.

##### (2) Questions
- `id`: A unique identifier for the question. This ID should be referenced in the appropriate category within the category section.
- `q-title`: The title of the question, which appears in the header of the FAQ card.
- `steps`: An array of step objects that provide step-by-step instructions for resolving the issue. These are the details that users will see when they open an FAQ card.
    - `s-title`: The title of the step, summarizing the action that should be taken.
    - `content`: An array of content objects that provide detailed instructions or media for the step.
        - `text`: The instructional text for the step.
        - `image`: The filename of the image to display (must be in .png format and located in /src/image/faq).
        - `size`: The size of the image, as defined in the FAQ image size configuration (Small, Medium, Large).
        - ⚠️ **Note**: If the object contains only text, the `text` key will be used exclusively. If an image is included, both `image` and `size` keys must be present to form an image object.

#### Image
- Location: `src/image/faq`
- Only `.png`, `.jpg`, and `.svg` files are supported.
- Image Size Configuration:
    - The size of the images displayed on the FAQ page can be configured in the `faq.json` file.
    - Three predefined sizes are available: **Small**, **Medium**, and **Large**. These sizes differ in height, with the width automatically adjusted to maintain the aspect ratio.
    - To ensure optimal display, **avoid uploading images that exceed the recommended width** for each size, as outlined in the table below:

| size   | height (fixed) | max-width |recommended ratio (h:w) | 
|:------:|:--------------:|:---------:|:----------------------:|
| Small  | 256px          | 1024px    |          < 4           |
| Medium | 384px          | 1024px    |          < 2.67        |
| Large  | 512px          | 1024px    |          < 2           |